from flask import Flask, render_template
import re

app = Flask(__name__)
valid_phones = []

def validate_phone(phone):
    if re.match(r"\d{10}$", phone):
        valid_phones.append(phone)
        return True
    return False

# Llama a la función validate_phone antes de iniciar la aplicación
validate_phone("1234567890")
validate_phone("1231567890")

@app.route("/")
def index():
    return render_template("index.html", phones=valid_phones)

if __name__ == "__main__":
    app.run(debug=True, host="0.0.0.0", port=5000)
